<?php

/**
 * @file
 * The jifupload uploader sends images to this file, which is a menu callback.
 *
 * Images are validated and saved, new nodes are pre-populated with default values.
 * Tokens are replaced if using token and filefield_paths.
 */

function jifupload_upload($type_name, $field_name, $nid) {

  if (!$_FILES) {
    return;
  }

  // We load the node we're editing and update any changed fields appearing above Aurigma
  // on the form. If there's no $nid node_save will assign a real nid, but we need to assign uid.
  $node = node_load($nid);

  // If it's a new node, we set the defaults for this content type.
  if ($nid == 0) {
    global $user;
    $node->uid = $user->uid;
    $node->type = $type_name;

    $options = variable_get('node_options_'. $type_name, '');
    if (in_array('promote', $options)) {
      $node->promote = 1;
    }
    if (in_array('status', $options)) {
      $node->status = 1;
    }
    if (in_array('sticky', $options)) {
      $node->sticky = 1;
    }

    // We do a preliminary node_save() so we get a real node id.
    node_save($node);
  }
  $node->title = ($node->title == $_POST['title'] ? $node->title : $_POST['title']);
  $node->body = ($node->body == $_POST['body'] ? $node->body : $_POST['body']);
  $node->format = ($node->format == $_POST['format'] ? $node->format : $_POST['format']);

  if (!$node->title) {
    form_set_error('title', $message = 'Please enter a title.', $reset = FALSE);
  }

  $images = array();
  $fileCount = count($_FILES) - 1;
  $dest = file_directory_temp();

  for ($i = 0; $i <= $fileCount; $i++) {
    $sourceFileField = "File" . $i;
    $filename = $_FILES[$sourceFileField]['tmp_name'];
    $handle = fopen($filename, "r");
    $data = fread($handle, filesize($filename));
    fclose($handle);

    $filepath = $dest .'/'. $_FILES[$sourceFileField]['name'];

    if ($filepath = file_save_data($data, $filepath)) {

      $file = new StdClass();
      $file->filepath = $filepath;
      $file->filename = $_FILES[$sourceFileField]['name'];
      $file->filemime = $_FILES[$sourceFileField]['type'];
      $file->filesize = $_FILES[$sourceFileField]['size'];

      // If we are using tokens, we replace the token and create the directory if necessary
      // Only checking for filefield_paths because it requires token.
      if (module_exists('filefield_paths')) {

        // We reuse some code from filefile paths module.
        $result = db_fetch_object(
          db_query("SELECT filename, filepath FROM {filefield_paths} WHERE type = '%s' AND field = '%s'", $node->type, $field_name)
        );
        $settings['filepath'] = unserialize($result->filepath);
        $filefield_path = $settings['filepath']['value'];

        // Add the tokens and other path info.
        $destination_path = $filefield_path;

        // Replace tokens with values.
        $destination_path = token_replace_multiple($destination_path, $types = array('node' => $node), $leading = '[', $trailing = ']', $options = array());
        jifupload_file_check_directories($destination_path, $mode = 1, $form_item = NULL);

        // Add back on the files directory before we save the file.
        $destination_path = file_directory_path() .'/'. $destination_path;

      }
      // If not using token we'll save to drupal's file directory.
      else {
        $destination_path = file_directory_path();
      }

      // Get the validators for this field
      $validators = array_merge(filefield_widget_upload_validators($field), imagefield_widget_upload_validators($field));

      // Save the new file
      if (! $file = field_file_save_file($filepath, $validators, $destination_path)) {
        watchdog('Aurigma Upload', 'Could not save file', NULL, WATCHDOG_ERROR);
        continue;
      }

      // Add the file info to the node.
      $node->{$field_name}[] = $file;

      // Delete the temp file.
      file_delete($filepath);
    }
    else {
      drupal_set_message($message = 'The file '. $file->filename .' could not be saved.', $type = 'status', $repeat = TRUE);
    }
  }

  // save the node
  node_save($node);
  // clear the cache
  cache_clear_all('content:'. $node->nid .':'. $node->vid, 'cache_content');

  echo "SUCCESS";
  exit;
}